﻿// ConsoleApplication6.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <cmath>

class MyClass
{
public:
	int GetTestVal() {
		return testVal;
	};

	int GetTestVal2() {
		return testVal2;
	};

private:
	int testVal = 0;
	int testVal2 = 1;
};

class Vector
{
public:
	Vector() {}

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {}

	void Show() {
		std::cout << '\n' << x << ' ' << y << ' ' << z;
	}

	double ModuleVector() {
		return sqrt(x * x + y * y + z * z);
	}

private:
	double x = 0;
	double y = 0;
	double z = 0;
};

int main()
{
    // std::cout << "Hello World!\n";

	MyClass a;
	std::cout << a.GetTestVal() << std::endl;
	std::cout << a.GetTestVal2() << std::endl;

	Vector v(10, 10, 10);
	std::cout << v.ModuleVector() << std::endl;

}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
